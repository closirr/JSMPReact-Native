import React from 'react';
import styles from './styles.js';
import { View, Image, FlatList, Text } from 'react-native';

export default class PostScreen extends React.Component {
    state = {post: {}};

    componentDidMount() {
        const { navigation } = this.props;
        const postId = navigation.getParam('postId');

        fetch('https://jsonplaceholder.typicode.com/posts/' + postId)
            .then(res => res.json())
            .then(post => this.setState({ post }))
            .catch(() => Alert.alert('aaaa'));
    }

    render() {
        return (
            <View styles={styles.container}>
                <Text style={styles.FontBase}> {this.state.post.title}</Text>
                <Text> {this.state.post.body}</Text>
            </View>
        )
    }
}

