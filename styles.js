import { StyleSheet, Text, View, Button, FlatList } from 'react-native';

export default styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
    FontBase: {
      fontSize: 30
    },
    PostItem: {
      borderWidth: 1,
      marginBottom: 30
    }
  });
  