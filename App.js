import React from 'react';
import { StyleSheet, Text, View, Animated, Button, FlatList } from 'react-native';
import { createStackNavigator, createMaterialTopTabNavigator } from 'react-navigation';
import styles from './styles.js';
import PostScreen from './PhotoList.js'

class PostsList extends React.Component {
  state = {
    posts: []
  }

  componentDidMount() {
    fetch('https://jsonplaceholder.typicode.com/posts')
      .then(res => res.json())
      .then(posts => this.setState({ posts }))
      .catch(() => Alert.alert('posts fetch err'));
  }

  render() {
    const { navigation } = this.props;
    return (
      <View style={styles.container}>
        <Text>Posts List</Text>
        <FlatList
          data={this.state.posts}
          renderItem={({ item }) =>
            <Post item={item} onPress={() =>{
              console.log(item.id);
              this.props.navigation.navigate('Details', { postId: item.id })}} />}
          keyExtractor={(item) => item.id.toString()}
        />
      </View>
    )
  }
}

class Post extends React.Component {
  render() {
    const { item, onPress } = this.props;
    return (
      <View style={[styles.container, styles.PostItem]} >
        <Text style={styles.FontBase}>{item.title}</Text>
        <Button
          title="watch post"
          onPress={() => onPress(item.id)}
        />
      </View>
    )
  }
}

export class App extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.FontBase}>Home</Text>
        <PostsList navigation={this.props.navigation}></PostsList>
      </View>
    );
  }
}

const homeStack = createStackNavigator({
  Home: App,
  Details: PostScreen
})



export default homeStack

